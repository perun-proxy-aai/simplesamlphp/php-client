## privacyIDEA PHP Client

This project is intended to ease the use of the privacyIDEA server REST API.

## Install

To install with Composer, run the following command:

`composer require cesnet/privacyidea-php-client`

## Disclaimer

This is a fork of [privacyidea/php-client](https://github.com/privacyidea/php-client) by [@privacyidea](https://github.com/privacyidea), maintained and contributed to by CESNET, z. s. p. o. and Institute of Computer Science, Masaryk University.
