<?php

declare(strict_types=1);

namespace PrivacyIDEA\PHPClient;

class PIBadRequestException extends \Exception
{
}
